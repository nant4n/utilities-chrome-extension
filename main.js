
////////////////////////////////////////////////////////////////////////////////////////////////////
// Canvas injection                                                                               //
////////////////////////////////////////////////////////////////////////////////////////////////////

const getBg = () => document.getElementById("annotator-extension-95813485793-h2g83g92h4gh-bg");
const canvasInjector = () => {
  if (document.getElementById("annotator-extension-95813485793-h2g83g92h4gh") !== null) return;
  
  const bg = document.createElement('div');
  bg.id = "annotator-extension-95813485793-h2g83g92h4gh-bg";
  bg.style.cursor = "none";
  bg.style.width = "100vw";
  bg.style.height = "100vh";
  bg.style.backgroundColor = "gray";
  bg.style.top = '0px';
  bg.style.position = "fixed";
  bg.style.opacity = 0.2;
  bg.style.pointerEvents = "none !important";
  bg.style.zIndex = 10001;

  const canvasElement = document.createElement('canvas');
  canvasElement.id = "annotator-extension-95813485793-h2g83g92h4gh-canvas";
  canvasElement.style.width = "100vw";
  canvasElement.style.height = "100vh";
  canvasElement.style.top = '0px';
  canvasElement.style.position = "fixed";
  canvasElement.style.zIndex = 10000;


  const container = document.createElement("div");
  container.id = "annotator-extension-95813485793-h2g83g92h4gh";
  container.style.width = "100vw";
  container.style.height = "100vh";
  container.style.top = '0px';
  container.style.position = "fixed";
  container.style.zIndex = 10000;

  container.appendChild(bg);
  container.appendChild(canvasElement);
  document.getElementsByTagName("body")[0].appendChild(container);

  const disableCanvasListener = (e) => {
    if (e.key === "Escape") {
      container.parentNode.removeChild(container);
    } else {
      document.addEventListener('keydown', (e) => disableCanvasListener(e), { once: true });
    }
  };
  document.addEventListener('keydown', (e) => disableCanvasListener(e), { once: true });
};



const getTabId = () => chrome.tabs.query({ active: true, currentWindow: true });
const injectCanvas = async () => {
  getTabId().then(res => {
    chrome.scripting.executeScript({
      target: {
        tabId: res[0].id
      },
      function: canvasInjector,
    });
  }).then(() => {
    //window.close();
  }).catch(err => alert(err));
};



const injectRemoveCanvas = async () => {
  getTabId().then(res => {
    chrome.scripting.executeScript({
      target: {
        tabId: res[0].id
      },
      function: () => {
        const e = document.getElementById("annotator-extension-95813485793-h2g83g92h4gh");
        e.parentNode.removeChild(e);
      },
    });
  }).then(() => {
    //window.close();
  }).catch(err => alert(err));
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// Draw button                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////

const drawButtonElement = document.getElementById("draw-button");
const draw = () => {
  injectCanvas();
  getTabId().then(res => {
    chrome.scripting.executeScript({
      target: {
        tabId: res[0].id
      },
      function: drawInjector,
    });
  }).then(() => {
    //window.close();
  }).catch(err => alert(err));
};

drawButtonElement.addEventListener('click', draw);


const drawInjector = () => {
  const cursor = document.getElementById("annotator-extension-95813485793-h2g83g92h4gh-cursor");
  if (cursor !== null) {
    cursor.parentNode.removeChild(cursor);
  }

  const cursorElement = document.createElement('div');
  cursorElement.style.position = "absolute";
  cursorElement.classList.add("dot-cursor");
  cursorElement.style.height = "10px";
  cursorElement.style.width = "10px";
  cursorElement.style.borderRadius = "50%";
  cursorElement.style.backgroundColor = "black";

  const cursorContainer = document.createElement("div");
  cursorContainer.id = "annotator-extension-95813485793-h2g83g92h4gh-cursor";
  cursorContainer.style.cursor = "none";
  cursorContainer.style.width = "100vw";
  cursorContainer.style.height = "100vh";
  cursorContainer.style.top = '0px';
  cursorContainer.style.position = "fixed";
  cursorContainer.style.zIndex = 10001;
  cursorContainer.appendChild(cursorElement);

  document.getElementById("annotator-extension-95813485793-h2g83g92h4gh").appendChild(cursorContainer);

  let isMouseDown = false;
  const canvas = document.getElementById("annotator-extension-95813485793-h2g83g92h4gh-canvas");
  
  const ctx = canvas.getContext("2d");
  cursorContainer.addEventListener("mousemove", (e) => {
    cursorElement.style.left = e.clientX + "px";
    cursorElement.style.top = e.clientY + "px";
    if (isMouseDown) {
      const coords = convertMouseToCanvas(e.clientX, e.clientY);
      // ctx.rect(coords.x, coords.y, 2, 2);
      // ctx.fill();
      ctx.lineTo(coords.x + 1, coords.y + 1); // Draw a line to (150, 100)
      ctx.stroke();
    }
  });

  const convertMouseToCanvas = (x, y) => {
    return { x: Math.round(x * canvas.width / window.innerWidth), y: Math.round(y * canvas.height / window.innerHeight) };
  };
  
  cursorContainer.addEventListener("mousedown", (e) => {
    isMouseDown = true;
    const coords = convertMouseToCanvas(e.clientX, e.clientY);
    ctx.beginPath();
    ctx.rect(coords.x, coords.y, 2, 2);
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(coords.x + 1, coords.y + 1);
  });
  cursorContainer.addEventListener("mouseup", () => {
    isMouseDown = false;
    ctx.stroke();
  });
};

